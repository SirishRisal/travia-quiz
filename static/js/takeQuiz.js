$(document).ready(function(){
  var username;
  var chatting_with;
  var gameId;
  var ongoingRound;
  var questionId;
  var listing={0:"A",1:"B",2:"C",3:"D"}
  var statusTrack={"true":"Ongoing","false":"Eliminated"}

  var socket =io.connect('//' + document.domain + ':' + location.port);
  $body = $("body");


  $('#user-name-submit').click(function(event){
    username = $('#user-name-field').val();
    if(username.length == 0) return false;
    $body.addClass("loading")
    socket.emit('new user', {username: username});
  }); 

  socket.on('player already exists', function(info){
    console.log('info', info);
    $body.removeClass("loading");
    alert(info)
    // $('.user-name').hide();
    // $('.main').show();
    // $("#username").html(username);
    // var question=JSON.parse(question)
    // gameId=question[0]["gameId"]
    // ongoingRound=question[0]["round"]
    // addQuestion(question)
    // countdown()
  });

  socket.on('start game', function(question){
    console.log('on quiz jion', question);
    $body.removeClass("loading");
    $('.user-name').hide();
    $('.main').show();
    $("#username").html(username);
    var question=JSON.parse(question)
    gameId=question[0]["gameId"]
    ongoingRound=question[0]["round"]
    addQuestion(question)
    countdown()
  });


socket.on('next round', function(question){
    console.log('question', question);
    var question=JSON.parse(question)
    $('#myModal').modal('hide');    
    ongoingRound=question[0]["round"]
    addQuestion(question)
    countdown()

  });


  socket.on('display results', function(results){
      console.log('results', results);
      $('#scoreBoard').html("");
      displayScore(results)    
    });

  socket.on('next round permission', function(results){
      if (results[username].hasOwnProperty(true)) {
          console.log("try for next round")
        if (checkWinner(results)) {
          $('#gameSummary').html("Congratulations!!! <br>" +username+", You won the game");
          $('#myModal').modal('show');
          socket.emit('end game', {"gameId":gameId});

        }
        else{
          socket.emit('new round', {gameId:gameId,ongoingRound:ongoingRound});
        }
      }
      else{
        $('#gameSummary').html("Game Over!! <br>Better luck next time "+username+".");
        $('#myModal').modal('show');
      }
    });




  function checkWinner(results){
    var winner=[]
    $.each(results, function(playername, details) { 
        $.each(details, function(status, score) { 
            winner.push(status)    
        });
    });
    var occurance=countElement('true',winner);
    if (winner.length>1 && occurance==1){
      return true
    }
    else{
      return false
    }
  }
        
      

  function countElement(item,array) {
      var count = 0;
      $.each(array, function(i,v) { if (v === item) count++; });
      return count;
  }


  function displayScore(data){
    // $('#scoreBoard').html("");
    var html="";
    $.each(data, function(playername, details) { 
        //alert(key + ': ' + value); 
                  $.each(details, function(status, score) { 
                //alert(key + ': ' + value); 
                         html+='<tr>\
                                      <td>'+playername+'</td>\
                                      <td>'+score+'</td>\
                                      <td>'+statusTrack[status]+'</td>\
                                  </tr>';

              });
      });
    $('#scoreBoard').html(html);
  }




  function countdown(){
      var time = 10;
      var initialOffset = '440';
      var i = 1
      /* Need initial run as interval hasn't yet occured... */
      $('.circle_animation').css('stroke-dashoffset', initialOffset-(1*(initialOffset/time)));
      var interval = setInterval(function() {
          $('#counter').text(secondsToTime(10-i).s.toString()+" sec");
          if (i == time) {    
            clearInterval(interval);
            // $('#myModal').modal('show');
            // $('#myModal').modal('hide');
            // socket.emit('new round', {gameId:gameId,ongoingRound:ongoingRound});
            // var questionId
            var values = [];
            $('input[type="checkbox"]:checked').each(function(i,v){
              var ans={"question":v.name,"answer":$(v).val()}
              values.push(ans);
            });
            
            if (values.length==1){
              var questionDetails=values[0]              
            }
            else{
              var questionDetails={"question":questionId,"answer":"undefined"}

            }
            questionDetails["gameId"]=gameId
            questionDetails["ongoingRound"]=ongoingRound
            questionDetails["player"]=username
            console.log(JSON.stringify('check answers '+questionDetails))
            socket.emit('check answers', questionDetails);
            return;
          }
          $('.circle_animation').css('stroke-dashoffset', initialOffset-((i+1)*(initialOffset/time)));
          i++;
      }, 1000);
  }
  



  function secondsToTime(secs){
      var hours = Math.floor(secs / (60 * 60));
      var divisor_for_minutes = secs % (60 * 60);
      var minutes = Math.floor(divisor_for_minutes / 60);
      var divisor_for_seconds = divisor_for_minutes % 60;
      var seconds = Math.ceil(divisor_for_seconds);
      var obj = {
          "m": minutes,
          "s": seconds
      };
      return obj;
  }


  function makeOptions(data,id,i){
    var htmlOption='<div class="form-check">\
          '+listing[i]+"."+'\
            <label class="form-check-label">\
                ' +data+'\
                <input class="form-check-input" required="" name="'+id+'" type="checkbox" value="'+data+'">\
                <span class="form-check-sign">\
                <span class="check"></span>\
                </span>\
            </label>\
        </div>';
        return(htmlOption)
  }


  function makeQuestion(data){
    questionId=data["questionId"]
    var html=""
    html+='<div>'
    html+='<h3>'+data["question"]+'?</h3>';
    html+='<div class="questionOptions">';
          for(j in data["options"]){
            html+=makeOptions(data["options"][j],data["questionId"],j)
          }
          html+='</div></form><hr>';
          return(html)
    }

  function addQuestion(x){
    var htmlInsert='<form method="post">';    
    htmlInsert+=makeQuestion(x[0]);
    $("#quizQuestions").html(htmlInsert);
    $("#questionRound").html('Round '+x[0]["round"].toString()+'.');
    $("input:checkbox").on('click', function() {
      // in the handler, 'this' refers to the box clicked on
        var $box = $(this);
        if ($box.is(":checked")) {
          // the name of the box is retrieved using the .attr() method
          // as it is assumed and expected to be immutable
          var group = "input:checkbox[name='" + $box.attr("name") + "']";
          // the checked state of the group/box on the other hand will change
          // and the current value is retrieved using .prop() method
          $(group).prop("checked", false);
          $box.prop("checked", true);
          $(group).prop("required", false);
        } else {
          $box.prop("checked", false);
          $box.prop("required", true);
        }
      });
    }


 

});

