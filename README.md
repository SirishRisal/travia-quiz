
Requirement

	Python3.5+ version


Installation:

	setup virtual environment for app
	activate virtual environment
	install requirement.txt

Run: 

	python main.py in terminal


	After building the app successfully browse http://127.0.0.1:5000/ and you are ready to go.

Description: 

	The game consists of multiple rounds. You can set game rounds by setting minimumQuestion 
	number in main.py. Currently there are 10 questions. In each round players are presented a 
	question, a	choice of answers, and have to select one answer within the allotted time (10 
	seconds or so). Players that chose a correct answer advance to the next round. Those who 
	chose the wrong answers are eliminated. The game continues until there’s only one winner 
	left.no admin intervention is required to start the game (once a minimum number of players 
	have joined) or advance the game to the next round.


	-Multiple games can run simultaneously.
	-large number of players to participate in each game (think hundreds) by setting 				  minimumPlayers in main.py.
	-update and displays statistics of the game after each round.

	
