
from flask import Flask,render_template,request,session,url_for
import requests
from flask_socketio import SocketIO, emit,join_room,send

import json
from datetime import datetime
import time,sched
import random
app = Flask(__name__)
socketio = SocketIO(app)

gameIdGlobal=""
allQuestions={}
ongoingGame={}
playerPoints={}
newPlayer=[]
minimumPlayers=2
minimumQuestion=10

@app.route('/',methods=['GET','POST'])
def index():
	return render_template("quiz.html")
			

@socketio.on('new user')
def new_user(message):
	global newPlayer
	global gameIdGlobal
	player = message['username']

	if player not in newPlayer:	
		if len(newPlayer)==0:
			gameIdGlobal=str(random.randint(0000,9999))
			playerPoints[gameIdGlobal]={}
			allQuestions[gameIdGlobal]={}
			create_questions_for_games(gameIdGlobal)
		
		newPlayer.append(player)
		ongoingGame[gameIdGlobal]={"questionsAsked":{},"players":newPlayer}
		playerPoints[gameIdGlobal]={player:{}}
		join_room(gameIdGlobal)
		emit('join quiz', {'player' :player, 'gameId':gameIdGlobal}, room = gameIdGlobal);	
		if len(newPlayer)==minimumPlayers:
			question=get_question(gameIdGlobal,0)
			emit('start game', question, room = gameIdGlobal,);
			newPlayer=[]	
	else:
		emit('player already exists', "Username already exists", broadcast= False,);

@socketio.on('new round')
def new_round(data):
	gameId=data["gameId"]
	ongoingRound=data["ongoingRound"]
	question=get_question(gameId,ongoingRound)
	emit('next round', question, broadcast=False,)


@socketio.on('check answers')
def check_answers(data):
	status=checkAnswer(data["question"],data["answer"],data["gameId"])
	playerPoints[data["gameId"]][data["player"]]={status:data["ongoingRound"]}
	print(playerPoints,"pp")
	emit('display results', playerPoints[data["gameId"]], room=data["gameId"],);
	emit('next round permission', playerPoints[data["gameId"]], broadcast=False,);
	

@socketio.on('end game')
def end_game(data):
	del ongoingGame[data["gameId"]]
	del allQuestions[data["gameId"]]
	del playerPoints[data["gameId"]]
	

def checkAnswer(question,answer,gameId):
	if allQuestions[gameId][question]["answer"]==answer:
		return True
	else:
		return False	

 
def create_questions_for_games(gameId):
	# URL="https://opentdb.com/api.php"
	# PARAMS={"amount":minimumQuestion,"type":"multiple"}
	# r = requests.get(url = URL, params = PARAMS) 
	# response=json.loads(r.text)["results"]
	with open('questions.json') as f:
	  response = json.load(f)["results"]

	allQuestions[gameId]["roundQuestionTracking"]={i+1:"" for i in range(0,minimumQuestion)}
	for i,j in enumerate(response):
		question=j["question"]
		options=j["incorrect_answers"]
		options.append(j["correct_answer"])
		answer=j["correct_answer"]
		questionId=str(random.randint(111,999))
		allQuestions[gameId][questionId]={'question':question,'options':options,"round":i+1,"answer":answer}
		allQuestions[gameId]["roundQuestionTracking"][i+1]=questionId
	return allQuestions


def get_question(gameId,ongoingRound):
	gameRound=ongoingRound+1
	questionId=allQuestions[gameId]["roundQuestionTracking"][gameRound]
	ongoingGame[gameId]["questionsAsked"][questionId]=gameRound
	question=allQuestions[gameId][questionId]["question"]
	options=allQuestions[gameId][questionId]["options"]
	
	# random.shuffle(options)     # uncomment for options to shuffle

	return json.dumps([{"gameId":gameId,'questionId': questionId, 'question':question,'options':options,"round":gameRound}])


if __name__ == '__main__':
    # app.run(debug=True,host='192.168.1.28',port=5000)
    socketio.run(debug=True)
    #socketio.run(app,host='192.168.1.28',port=5000,debug=True)
